package Task1;

public class Task1b_AreEqual {

	public static void main(String[] args) {
		System.out.println(areEqual(2,1));
	}
	
	public static boolean areEqual(int a, int b){
		return a == b;
	}
	
}
